#How to use in your project

##Include in your composer.json

    "repositories": [
    {
        "type": "vcs",
        "url": "https://dsahapersonal@bitbucket.org/dsahapersonal/pdoconnect.git",
            "autoload": {
                "psr-4" : {
                    "dsahapersonal\\pdoconnect" : "src"
                }
            }
        }
    ],
      "require": {
        "dsahapersonal/pdoconnect": "dev-master"
    }

#Usage

    use dsahapersonal\pdoconnect\Handler;

    $con = new Handler();